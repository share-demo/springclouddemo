# 消费者服务
- 用于调用其他已注册的服务

## 技术点
#### 提供API调用服务
- 提供restful风格API调用服务
### 使用feign调用其他服务
- 使用@EnableFeignClients注解
- 使用方法，调用其他`已注册服务`并增加`熔断器`

```
/**
 * 参数解释：</br>
 * 1.name:调用服务的服务名</br>
 * 2.fallback：调用服务失败后，执行的类
 * @author xiaofeng
 * @date 2018/2/27
 */
@FeignClient(name = "service-system",fallback = UserHystrixClientFallback.class)
public interface UserHystrixClient {
	@RequestMapping(value = "/hi",method = RequestMethod.GET)
	public String sayHi(@RequestParam("userName") String userName);
}
```

### 熔断器hystrix
- feign已整合熔断器hystrix无需另外配置

### 负载均衡
- feign已整合hystrix与ribbon（负载均衡）无需另外配置

### 能够被hystrixdashboard监控到
- 使用@EnableHystrix注解
- 配置两个依赖使用

```
<!--以下两个依赖为了hystrix监控使用，图形-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>

    <!-- 整合hystrix，其实feign中自带了hystrix，引入该依赖主要是为了使用其中的hystrix-metrics-event-stream，用于dashboard -->
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-hystrix</artifactId>
    </dependency>
```