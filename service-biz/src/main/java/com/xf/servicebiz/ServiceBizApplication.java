package com.xf.servicebiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

/**
 * @author xiaofeng
 * 启用服务消费者feign</br>
 * feign集成了ribbon，来提供负载均衡</br>
 * EnableHystrix 开启断路器功能，虽然feign集成了Hystrix，如果需要开启Hystrix监控需要加上这个注解，不需要可以去掉
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableHystrix
public class ServiceBizApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceBizApplication.class, args);
	}
}
