package com.xf.servicebiz.service;

import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author xiaofeng
 * @date 2018/2/27
 */
public interface UserService {

	public String sayHi(@RequestParam String username);
}
