package com.xf.servicebiz.service.impl;

import com.xf.servicebiz.client.UserHystrixClient;
import com.xf.servicebiz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author xiaofeng
 * @date 2018/2/27
 */
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserHystrixClient userHystrixClient;

	@Override
	public String sayHi(String username) {
		return userHystrixClient.sayHi(username);
	}
}
