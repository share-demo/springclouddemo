package com.xf.servicebiz.controller;

import com.xf.servicebiz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@Autowired
	private UserService userService;

	@GetMapping("/hi")
	public String sayHi(@RequestParam String userName){
		return "hi "+userName+" from biz service";
	}

	@GetMapping("/hello")
	public String hello(@RequestParam("userName") String userName){
		return userService.sayHi(userName);
	}
}
