package com.xf.servicebiz.client.fallback;

import com.xf.servicebiz.client.UserHystrixClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 调用服务失败后，执行的类
 *
 * @author xiaofeng
 * @date 2018/2/27
 */
@Component
public class UserHystrixClientFallback implements UserHystrixClient {
	Logger logger = LoggerFactory.getLogger(UserHystrixClientFallback.class);

	@Override
	public String sayHi(String userName) {
		logger.error("异常发生，进入service-biz服务sayHi的fallback方法，接收的参数:username={}", userName);
		return "异常发生，进入fallback方法";
	}
}
