package com.xf.servicebiz.client;

import com.xf.servicebiz.client.fallback.UserHystrixClientFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 参数解释：</br>
 * 1.name:调用服务的服务名</br>
 * 2.fallback：调用服务失败后，执行的类
 * @author xiaofeng
 * @date 2018/2/27
 */
@FeignClient(name = "service-system",fallback = UserHystrixClientFallback.class)
public interface UserHystrixClient {

	@RequestMapping(value = "/hi",method = RequestMethod.GET)
	public String sayHi(@RequestParam("userName") String userName);
}
