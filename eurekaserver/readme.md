# 注册中心
-  [原版官网](https://cloud.spring.io/spring-cloud-static/Finchley.M6/single/spring-cloud.html)
-  [中文版](https://springcloud.cc/spring-cloud-dalston.html)

## 简单配置方法(12.4 Standalone Mode)

```
spring:
    application:
      name: spring-cloud-eureka
server:
  port: 1111
eureka:
  instance:
    hostname: localhost
  client:
    registerWithEureka: false
    fetchRegistry: false
    serviceUrl:
      defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka/
  ```
## 高可用集群配置(12.5 Peer Awareness)

```
#改成3个- 
spring:
  application:
    name: spring-cloud-eureka
  profiles: peer1
server:
  port: 8000
eureka:
  instance:
    # 必须使用自身的hostname
    hostname: peer1
  client:
    serviceUrl:
      # 这里的url需要使用需要高可用的hostname和port
      defaultZone: http://peer2:8001/eureka/
      
#改成3个-  
spring:
  application:
    name: spring-cloud-eureka
  profiles: peer2
server:
  port: 8001
eureka:
  instance:
   #必须使用自身的hostname
    hostname: peer2
  client:
    serviceUrl:
     # 这里的url需要使用需要高可用的hostname和port
      defaultZone: http://peer1:8000/eureka/
      
```
- 示意图
![](image/1.png)