package com.xf.servicesystem.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

@RestController
public class SystemController {
	Logger logger = LoggerFactory.getLogger(SystemController.class);

	@GetMapping("/hi")
	public String sayHi(@RequestParam("userName") String userName){
		logger.info("进入system-service sayHi方法");
		return "hi "+userName+" from system service";
	}
}
