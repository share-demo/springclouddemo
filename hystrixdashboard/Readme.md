[TOC]

# hystrix-dashboard

## 作用
- 实时可视化查看hytrix监控数据

## 配置
- 无需注册到Eureka,可单独使用

### yml：
```
spring:
  application:
    name: hystrix-dashboard
server:
  port: 9999
```
### pom
```
  <dependencies>
    <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-hystrix-dashboard</artifactId>
    </dependency>
    <!--图形界面-->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
  </dependencies>
```
### 修改启动类
```
@SpringBootApplication
@EnableHystrixDashboard
public class HystrixdashboardApplication {

	public static void main(String[] args) {
		SpringApplication.run(HystrixdashboardApplication.class, args);
	}
}
```

### 需要监控的服务的类的修改
#### 需要监控的服务的pom
```
    <!--以下两个依赖为了hystrix监控使用，图形-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>

    <!-- 整合hystrix，其实feign中自带了hystrix，引入该依赖主要是为了使用其中的hystrix-metrics-event-stream，用于dashboard -->
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-hystrix</artifactId>
    </dependency>
```
#### 需要监控的服务的启动类
```
/**
 * @author xiaofeng
 * 启用服务消费者feign</br>
 * feign集成了ribbon，来提供负载均衡</br>
 * EnableHystrix 开启断路器功能，虽然feign集成了Hystrix，如果需要开启Hystrix监控需要加上这个注解，不需要可以去掉
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableHystrix
public class ServiceBizApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceBizApplication.class, args);
	}
}
```


### 使用方法
- 访问dashboard服务，http://127.0.0.1:9999/hystrix.stream，填入需要监控的地址
![](image/1.png)

- 结果如下图
![](image/2.png)